print("New Vala Project")
print("This program will setup a new vala project using meson")
print("-------------------------------------------------------")

local runscript_template = [[
#!/bin/bash
if [ -f foo ]
then
	mv -f <name> <name>.old
fi
./build && ./<name>
]]

local build_template = [[
#!/bin/bash
builddir=meson_build
output="src/<reverse_domain><name>"
dest="<name>"

if [ ! -d "$builddir" ]
then
	meson "$builddir" --prefix=/usr
fi

cd "$builddir"
rm -f "$output"
ninja
cp -f "$output" "../$dest"
]]

local meson_build_root_template = [[
project('<reverse_domain><name>','vala','c')

subdir('src')
]]

local meson_build_src = [[
	executable(
	meson.project_name(),
	
	##_FILES_##
	'main.vala', #AUTOUPDATE_FILE
	
	dependencies: [
		dependency('glib-2.0'),
	],
	install: true
)
]]

local update_src_build_files_lua = [[
#!/usr/bin/lua5.3

files = {}

p = io.popen("find src/ -name \"*.vala\" | cut -d/ -f2-")

while true do
	local i = p:read()
	if not i then break end
	files[#files+1] = i
end

p:close()

function compare_files(a,b)
	local as = not not a:find("/",1,true)
	local bs = not not b:find("/",1,true)
	if not (as == bs) then
		return not as
	end
	return a < b
end

table.sort(files,compare_files)

function print_files()
	for _,f in pairs(files) do
		print("\t'"..f:gsub("'","\\'").."', #AUTOUPDATE_FILE")
	end
end


p = io.popen("cat src/meson.build | grep -v -F \"#AUTOUPDATE_FILE\"")
while true do
	local i = p:read()
	if not i then break end
	print(i)
	if i:find("##_FILES_##",1,true) then
		print_files();
	end	
end

p:close()
]]

local update_src_build_files_sh = [[
#!/bin/bash

#This script automatically finds all .vala files in the src/ folder and adds them to the src/meson.build file
#If you didn't add or (re)move files you don't need this script

lua tools/update_src_build_files.lua > src/meson.build.new
mv -f src/meson.build src/meson.build.old
mv -f src/meson.build.new src/meson.build
]]

local main_vala_template = [[
public static int main(string[] args) {
	// TODO
	print("Hello world from <name>!\n");
	return 1;
}
]]

local bulksed_lua = [[
sed = ...

if not sed then
	print("Usage:")
	print("  bulksed <sed command>")
	print("  Handle with care and have a backup ready!")
end

while true do
	local i = io.read();
	if not i then break end
	os.execute("sed -i '"..sed.."' '"..i.."'")
end
]]

local rename_sh = [[
#!/bin/bash
find -name "*.vala" | lua tools/bulksed.lua "s/$1/$2/g"
]]

local gitignore_template = [[
meson_build
<name>
<name>.old
src/meson.build.old
setup_project.lua
]]

local readme_template = [[
# <name>

TODO: Put a description of the project here

## How to build?

### Dependencies
- A working vala compiler
- glib-2.0
- meson
- TODO

If you wannt to use all the scripts (not just `build` and `run`) you also need lua 5.x installed

A guaranteed up to date list of dependencies can be found in the src/meson.build file.

If you added or removed source files use the `update_src_build_files` script, it will atomatically update the src/meson.build file so you don't have to do anything.

### Building and running
To build it, run the `build.sh` script, which will automatically setup
the build folder, run ninja, and put the output in the projects root
directory. The produced binary should be executable, now.

To make development easy, the `run` script calls the `build.sh` script
and then runs whatever is at the output (it will try and fail if there is none).
]]

local function untemplate(template, template_info)
	return template:gsub("<name>",template_info.name):gsub("<reverse_domain>",template_info.reverse_domain)
end

local function ask(question)
	print(question)
	io.write("> ")
	return io.read()
end

local function ask_yn(question)
	return ask(question.." (y/N)") == "y"
end

local function printif(text, condition)
	if condition then
		print(text)
	end
end

local function gather_template_info()
	local name = ask("Please enter the name of your project")

	if not name or name == "" then
		print("Name empty, doing nothing")
		os.exit()
	end

	local reverse_domain = ask("Please enter the reverse domain name of the projects home") or ""

	if reverse_domain ~= "" then
		reverse_domain = reverse_domain:gsub(" ", "-").."."
	end
	name = name:gsub(" ", "-")
	
	local template_info = {
		name = name,
		reverse_domain = reverse_domain,
	}
	
	template_info.generate_main = ask_yn("Create a main.vala file?")
	print("rename.sh is a script that is able to run a single sed command across every *.vala file in the src/ directory replacing anthing with everything, wich is useful for renaming classes and interfaces but can also do a very excellent job at ruining your day without asking if you screw up and don't have a backup!")
	print("-- If you enable it make sure you have a versioning system ready!! --")
	template_info.add_bulksed = ask_yn("Add rename.sh? (Use a versioning/backup system!)")
	template_info.init_git = ask_yn("Initialize git repository?")
	if template_info.init_git then
		if ask_yn("Add a remote git repository?") then
			template_info.git_remote = ask("Ok, create your remte repo and give me the (clone) address.")
		end
	end
	return template_info
end

local function ask_correct(template_info)
	print("-------------------------------------------------------")
	print("Project Executable Name: "..template_info.name)
	print("Project id: "..template_info.reverse_domain..template_info.name)
	print("Local project folder: "..os.getenv("PWD"))
	printif("+ Add a main.vala file", template_info.generate_main)
	printif("+ Add the rename.sh bulksed script", template_info.add_bulksed)
	if template_info.init_git then
		if template_info.git_remote then
			print("+ Initialize a git repository with remote "..template_info.git_remote)
		else
			print("+ Initialize a git repository")
		end
	end
	return ask_yn("Is this information correct?")
end

local function write_file(path, content)
	io.write("Creating file "..path.." ")
	local f = io.open(path, "w")
	f:write(content)
	f:close()
	io.write("✔\n")
end

local function chmodx(path)
	print("Setting Executable flag on "..path)
	os.execute("chmod +x "..path)
end

local function mkdir(path)
	print("Creating "..path.." directory")
	os.execute("mkdir -p "..path)
end

local function create_project(template_info)
	print("-------------------------------------------------------")
	mkdir("src")
	mkdir("tools")
	write_file("run", untemplate(runscript_template, template_info))
	chmodx("run")
	write_file("build", untemplate(build_template, template_info))
	chmodx("build")
	write_file("meson.build", untemplate(meson_build_root_template, template_info))
	write_file("src/meson.build", untemplate(meson_build_src, template_info))
	write_file("tools/update_src_build_files.lua", untemplate(update_src_build_files_lua, template_info))
	write_file("update_src_build_files", untemplate(update_src_build_files_sh, template_info))
	chmodx("update_src_build_files")
	write_file("README.md", untemplate(readme_template, template_info))
	if template_info.generate_main then
		write_file("src/main.vala", untemplate(main_vala_template, template_info))
	end
	if template_info.add_bulksed then
		write_file("tools/bulksed.lua", bulksed_lua)
		write_file("rename.sh", rename_sh)
	end
	if template_info.init_git then
		write_file(".gitignore", untemplate(gitignore_template, template_info))
		print("Initializing git repository")
		os.execute("git init")
		print("Making initial commit")
		os.execute("git add .")
		os.execute("git commit -m \"Project "..template_info.name.." created!\"")
		if template_info.git_remote then
			print("Adding git remote (You have to make the first push yourself)")
			os.execute("git remote add origin '"..template_info.git_remote.."'")
			os.execute("git remote -v")
		end
	end
	print("-------------------------------------------------------")
end

local function main()
	local template_info = gather_template_info()
	if ask_correct(template_info) then
		print("Ok, I'm creating the project!")
		create_project(template_info)
	else
		print("Please restart the script then.")
	end
end

main()
